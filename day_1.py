import argparse
from typing import TextIO


def part_1(input_file: TextIO):
    input_text = input_file.read()
    increase_count = 0
    previous_measurement = None

    for measurement in input_text.split('\n'):
        measurement = int(measurement)
        if previous_measurement is None:
            previous_measurement = measurement
            continue
        if measurement > previous_measurement:
            increase_count += 1
        previous_measurement = measurement

    return increase_count


def part_2(input_file: TextIO):
    input_text = input_file.read()

    increase_count = 0

    window = []
    previous_sum = None
    for measurement in input_text.split('\n'):
        measurement = int(measurement)
        window.append(measurement)
        if len(window) < 3:
            continue
        elif len(window) == 3:
            pass
        else:
            window.pop(0)

        window_sum = sum(window)

        if previous_sum is None:
            previous_sum = window_sum
            continue

        if window_sum > previous_sum:
            increase_count += 1
        previous_sum = window_sum

    return increase_count


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 1')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
